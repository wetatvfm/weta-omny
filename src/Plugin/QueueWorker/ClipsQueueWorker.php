<?php

namespace Drupal\weta_omny\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\weta_omny\ClipManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Clips data from the Omny Studio API to Audio Program
 * Episode nodes.
 *
 * Queue items are added by cron processing.
 *
 * @QueueWorker(
 *   id = "weta_omny.queue.clips",
 *   title = @Translation("Omny Studio Clips processor"),
 *   cron = {"time" = 60},
 *   weight = 10
 * )
 *
 * @see weta_omny_cron()
 * @see \Drupal\Core\Annotation\QueueWorker
 * @see \Drupal\Core\Annotation\Translation
 */
class ClipsQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Clip manager.
   *
   * @var \Drupal\weta_omny\ClipManager
   */
  private ClipManager $clipManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    ClipManager $clip_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->clipManager = $clip_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): ClipsQueueWorker|ContainerFactoryPluginInterface|static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('weta_omny.clip_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $this->clipManager->addOrUpdateClip($data);
  }

}

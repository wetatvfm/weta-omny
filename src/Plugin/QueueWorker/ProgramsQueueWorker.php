<?php

namespace Drupal\weta_omny\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\weta_omny\ProgramManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Playlist data from the Omny Studio API to Program nodes.
 *
 * Queue items are added by cron processing.
 *
 * @QueueWorker(
 *   id = "weta_omny.queue.programs",
 *   title = @Translation("Omny Studio Programs processor"),
 *   cron = {"time" = 60},
 *   weight = -10
 * )
 *
 * @see weta_omny_cron()
 * @see \Drupal\Core\Annotation\QueueWorker
 * @see \Drupal\Core\Annotation\Translation
 */
class ProgramsQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {


  /**
   * Program manager.
   *
   * @var \Drupal\weta_omny\ProgramManager
   */
  private ProgramManager $programManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    ProgramManager $program_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->programManager = $program_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): ProgramsQueueWorker|ContainerFactoryPluginInterface|static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('weta_omny.program_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $this->programManager->addOrUpdateAudioProgram($data);
  }

}

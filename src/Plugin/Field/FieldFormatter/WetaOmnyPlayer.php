<?php

namespace Drupal\weta_omny\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Formatter that embeds the Omny Player.
 *
 * @FieldFormatter(
 *   id = "weta_omny_player",
 *   label = @Translation("Embedded Omny Player"),
 *   field_types = {
 *     "link",
 *     "string",
 *   }
 * )
 */
class WetaOmnyPlayer extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    // Get the entity title.
    $title = $items->getEntity()->label();

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'weta_omny_embed',
        '#src' => $item->getFieldDefinition()->getType() === 'link'
          ? $item->uri
          : $item->value,
        '#title' => $title,
      ];
    }

    return $elements;
  }

}

<?php

namespace Drupal\weta_omny\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Formatter that embeds the Omny Player.
 *
 * @FieldFormatter(
 *   id = "weta_omny_playlist",
 *   label = @Translation("Embedded Omny Playlist"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class WetaOmnyPlaylist extends FormatterBase {

  /**
   * {@inheritdoc}
   *
   * Only show this formatter if the name of the field refers to an Omny slug.
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    $field_name = $field_definition->getName();
    return parent::isApplicable($field_definition) && $field_name == 'field_omny_embed';
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    // Get the entity title.
    $title = $items->getEntity()->label();

    foreach ($items as $delta => $item) {

      $elements[$delta] = [
        '#theme' => 'weta_omny_playlist_embed',
        '#src' => $item->value,
        '#title' => $title
      ];
    }

    return $elements;
  }

}

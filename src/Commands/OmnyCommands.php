<?php

namespace Drupal\weta_omny\Commands;

use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\RequeueException;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\weta_omny\ProgramManager;
use Drupal\weta_omny\ClipManager;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file.
 */
class OmnyCommands extends DrushCommands {

  /**
   * The Omny Studio program manager.
   *
   * @var \Drupal\weta_omny\ProgramManager
   */
  protected ProgramManager $programManager;

  /**
   * The Omny Studio clip manager.
   *
   * @var \Drupal\weta_omny\ClipManager
   */
  protected ClipManager $clipManager;

  /**
   * The queue worker manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected QueueWorkerManagerInterface $workerManager;

  /**
   * The queue service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected QueueFactory $queueService;

  /**
   * Constructs a new DrushCommands object.
   *
   * @param \Drupal\weta_omny\ProgramManager $program_manager
   *   The Omny Studio program manager.
   * @param \Drupal\weta_omny\ClipManager $clip_manager
   *   The Omny Studio clip manager.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $worker_manager
   *   The queue worker manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_service
   *   The queue service.
   */
  public function __construct(ProgramManager $program_manager, ClipManager $clip_manager, QueueWorkerManagerInterface $worker_manager, QueueFactory $queue_service) {
    parent::__construct();
    $this->programManager = $program_manager;
    $this->clipManager = $clip_manager;
    $this->workerManager = $worker_manager;
    $this->queueService = $queue_service;
  }

  /**
   * Command to pull a single Omny Studio program/playlist.
   *
   * @param string $playlist_id
   *   The Omny Studio id of the playlist.
   * @param array $options
   *   Associative array of options.
   *
   * @command weta_omny:getPlaylist
   * @aliases omny-gp
   */
  public function getPlaylist(string $playlist_id, array $options = ['force' => FALSE]) {
    $playlist = $this->programManager->getPlaylist($playlist_id);
    if (is_object($playlist)) {
      $this->programManager->addOrUpdateAudioProgram($playlist, $options['force']);
    }
    else {
      $this->logger->error(dt('The playlist ID @id did not return any results.', [
        '@id' => $playlist_id,
      ]));
    }
  }

  /**
   * Command to pull a single Omny Studio clip.
   *
   * @param string $clip_id
   *   The Omny Studio id of the clip.
   * @param array $options
   *   Associative array of options.
   *
   * @command weta_omny:getClip
   * @aliases omny-gc
   */
  public function getClip(string $clip_id, array $options = ['force' => FALSE]) {
    $clip = $this->clipManager->getClip($clip_id);
    if (is_object($clip)) {
      $this->clipManager->addOrUpdateClip($clip, $options['force']);
    }
    else {
      $this->logger->error(dt('The clip ID @id did not return any results.', [
        '@id' => $clip_id,
      ]));
    }
  }

  /**
   * Command to add all (subscribed) playlists to the Drupal queue.
   *
   * @command weta_omny:getAllPlaylists
   * @aliases omny-gap
   */
  public function getAllPlaylists() {
    $this->programManager->updateQueue();
    $this->output()->writeln(dt('Process the playlists with `drush queue:run weta_omny.queue.programs` or run cron.'));
  }

  /**
   * Command to add all (subscribed) clips to the Drupal queue.
   *
   * @command weta_omny:getAllClips
   * @aliases omny-gac
   */
  public function getAllClips() {
    $this->clipManager->updateQueue();
    $this->output()->writeln(dt('Process the clips with `drush queue:run weta_omny.queue.clips` or run cron.'));
  }

  /**
   * Command to force all (subscribed) clips to update.
   *
   * @command weta_omny:updateAllClips
   * @aliases omny-uac
   */
  public function updateAllClips() {
    $this->clipManager->forceUpdateAllClips();
  }

  /**
   * Command to process all (subscribed) playlists and clips.
   *
   * @command weta_omny:processAll
   * @aliases omny-pa
   */
  public function processAll() {
    $this->programManager->updateQueue();
    $this->runQueue('weta_omny.queue.programs');
    $this->clipManager->updateQueue();
    $this->runQueue('weta_omny.queue.clips');
    $this->output()->writeln(dt("All playlists and clips have been processed."));
  }

  /**
   * Run a specific queue by name.
   *
   * @param string $name
   *   The name of the queue to run.
   *
   * @command weta_omny:rq
   * @aliases omny-rq
   * @throws \Exception
   */
  public function runQueue($name) {
    $worker = $this->workerManager->createInstance($name);
    $queue = $this->queueService->get($name);
    $start = microtime(TRUE);
    $count = 0;

    while ($item = $queue->claimItem()) {
      try {
        $this->logger()->info(dt('Processing item @id from @name queue.', [
          '@name' => $name,
          '@id' => $item->item_id,
        ]));
        $worker->processItem($item->data);
        $queue->deleteItem($item);
        $count++;
      }
      catch (RequeueException) {
        // The worker requested the task to be immediately re-queued.
        $queue->releaseItem($item);
      }
      catch (SuspendQueueException $e) {
        // If the worker indicates there is a problem with the whole queue,
        // release the item.
        $queue->releaseItem($item);
        throw new \Exception($e->getMessage());
      }
    }
    $elapsed = microtime(TRUE) - $start;
    $this->logger()->success(dt('Processed @count items from the @name queue in @elapsed sec.', [
      '@count' => $count,
      '@name' => $name,
      '@elapsed' => round($elapsed, 2),
    ]));
  }

}

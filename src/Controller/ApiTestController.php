<?php

namespace Drupal\weta_omny\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\weta_omny\ApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Test controller for the weta_omny module.
 */
class ApiTestController extends ControllerBase {

  /**
   * The Omny Studio client.
   *
   * @var \Drupal\weta_omny\ApiClient
   */
  protected ApiClient $client;



  /**
   * Constructs the ApiTestController.
   *
   * @param \Drupal\weta_omny\ApiClient $api_client
   *   The Omny Studio API service.
   */
  public function __construct(ApiClient $api_client) {
    $this->client = $api_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ApiTestController|static {
    return new static(
      $container->get('weta_omny.api_client')
    );
  }

  /**
   * Sends a test query to the API and returns result.
   *
   * @throws \Exception
   */
  public function testConnection(): array {

    $response = $this->client->testConnection();

    $message = $response;
    if ($response == 'OK') {
      $message = 'The API test succeeded.';
    }

    return [
      '#theme' => 'item_list',
      '#title' => 'Test Result',
      '#items' => [$message],
    ];
  }

}

<?php

namespace Drupal\weta_omny\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\weta_omny\ApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OmnyStudioSettingsForm.
 */
class OmnyStudioSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'weta_omny.settings';

  /**
   * The API client.
   *
   * @var \Drupal\weta_omny\ApiClient
   */
  protected ApiClient $apiClient;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Date formatting service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;


  public function __construct(
    ConfigFactory $config_factory,
    ApiClient $api_client,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    parent::__construct($config_factory);
    $this->apiClient = $api_client;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): OmnyStudioSettingsForm|ConfigFormBase|static {
    return new static(
      $container->get('config.factory'),
      $container->get('weta_omny.api_client'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }


  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'omnystudio_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::SETTINGS);
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('Omny Studio API key'),
      '#maxlength' => 128,
      '#size' => 60,
      '#default_value' => $config->get('api.key'),
    ];

    $form['api']['base_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base URI'),
      '#description' => $this->t('Omny Studio API base URI.'),
      '#maxlength' => 128,
      '#size' => 60,
      '#default_value' => $config->get('api.base_uri'),
    ];

    $form['api']['consumer_base_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Consumer base URI'),
      '#description' => $this->t('Omny Studio Consumer API base URI.'),
      '#maxlength' => 128,
      '#size' => 60,
      '#default_value' => $config->get('api.consumer_base_uri'),
    ];

    $form['api']['consumer_base_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Consumer base endpoint'),
      '#description' => $this->t('Omny Studio Consumer API base endpoint.'),
      '#maxlength' => 128,
      '#size' => 60,
      '#default_value' => $config->get('api.consumer_base_endpoint'),
    ];

    $form['api']['organization_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Organization ID'),
      '#description' => $this->t('Omny Studio organization ID. (Required to retrieve transcripts.)'),
      '#maxlength' => 128,
      '#size' => 60,
      '#default_value' => $config->get('api.organization_id'),
    ];

    $form['api']['disable_notices'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable notices'),
      '#default_value' => $config->get('api.disable_notices'),
      '#description' => $this->t('If checked, notices that items have been created, updated, skipped, or ignored will not be logged. Errors and warnings will still be recorded.'),
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('api.key', $form_state->getValue('api_key'))
      ->set('api.base_uri', $form_state->getValue('base_uri'))
      ->set('api.consumer_base_uri', $form_state->getValue('consumer_base_uri'))
      ->set('api.consumer_base_endpoint', $form_state->getValue('consumer_base_endpoint'))
      ->set('api.organization_id', $form_state->getValue('organization_id'))
      ->set('api.disable_notices',  $form_state->getValue('disable_notices'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}

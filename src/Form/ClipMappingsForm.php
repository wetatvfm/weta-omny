<?php

namespace Drupal\weta_omny\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\weta_omny\ApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ClipMappingsForm.
 *
 * @ingroup weta_omny
 */
class ClipMappingsForm extends ConfigFormBase {

  /**
   * Omny Studio API client.
   *
   * @var \Drupal\weta_omny\ApiClient
   */
  protected ApiClient $apiClient;

  /**
   * Date formatting service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new OmnyStudioSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory service.
   * @param \Drupal\weta_omny\ApiClient $api_client
   *   Omny Studio API client service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    ConfigFactory $config_factory,
    ApiClient $api_client,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    parent::__construct($config_factory);
    $this->apiClient = $api_client;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ClipMappingsForm|ConfigFormBase|static {
    return new static(
      $container->get('config.factory'),
      $container->get('weta_omny.api_client'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'omnystudio_clip_mappings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['weta_omny.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('weta_omny.settings');
    $clip_content_type = $config->get('clips.drupal_clip_content');

    $form['clip_content_type'] = [
      '#type' => 'details',
      '#title' => $this->t('Clip content type'),
    ];
    $drupal_content_types = array_keys($this->entityTypeManager->getStorage('node_type')->loadMultiple());
    $content_type_options = array_combine($drupal_content_types, $drupal_content_types);
    $form['clip_content_type']['drupal_clip_content'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal clip content type'),
      '#description' => $this->t('Select the Drupal content type to use for Omny Studio clip content.'),
      '#default_value' => $config->get('clips.drupal_clip_content'),
      '#options' => $content_type_options,
    ];

    $form['clip_field_mappings'] = [
      '#type' => 'details',
      '#title' => $this->t('Clip field mappings'),
    ];
    $clip_field_options = ['unused' => 'unused'];
    if (!empty($config->get('clips.drupal_clip_content'))) {
      $required_clip_fields = [
        'Id',
        'Title',
        'Slug',
      ];
      $clip_fields = array_keys($this->entityFieldManager
        ->getFieldDefinitions('node', $clip_content_type));
      $clip_field_options += array_combine($clip_fields, $clip_fields);
      $omny_clip_fields = $config->get('clips.mappings');
      foreach (array_keys($omny_clip_fields) as $field_name) {
        $required = in_array($field_name, $required_clip_fields);
        $form['clip_field_mappings'][$field_name] = [
          '#type' => 'select',
          '#title' => $field_name,
          '#options' => $clip_field_options,
          '#required' => $required,
          '#default_value' => $omny_clip_fields[$field_name],
        ];
      }
    }
    else {
      $form['clip_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save Drupal clip content type (above) to choose field mappings.',
      ];
    }

    // Image media type configuration.
    $form['image_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Image settings'),
    ];
    $media_types = array_keys($this->entityTypeManager->getStorage('media_type')->loadMultiple());
    $media_type_options = array_combine($media_types, $media_types);
    $image_media_type = $config->get('clips.image_media_type');
    $form['image_settings']['image_media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal image media type'),
      '#default_value' => $image_media_type,
      '#options' => $media_type_options,
    ];
    // Media image field mappings.
    if (!empty($image_media_type)) {
      $form['image_settings']['image_field_mappings'] = [
        '#type' => 'details',
        '#title' => $this->t('Image field mappings'),
        '#open' => TRUE,
      ];
      $image_media_fields = array_keys($this
        ->entityFieldManager
        ->getFieldDefinitions('media', $image_media_type));
      $image_field_options = ['unused' => 'unused'] +
        array_combine($image_media_fields, $image_media_fields);
      $omny_image_fields = $config->get('clips.image_field_mappings');
      foreach ($omny_image_fields as $omny_image_field => $field_value) {
        $form['image_settings']['image_field_mappings'][$omny_image_field] = [
          '#type' => 'select',
          '#title' => $omny_image_field,
          '#options' => $image_field_options,
          '#default_value' => $field_value,
          '#required' => TRUE,
        ];
      }
    }
    else {
      $form['image_settings']['image_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save the Drupal image media type to choose field mappings.',
      ];
    }

    // Audio media type configuration.
    $form['audio_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Audio settings'),
    ];
    $media_types = array_keys($this->entityTypeManager->getStorage('media_type')->loadMultiple());
    $media_type_options = array_combine($media_types, $media_types);
    $audio_media_type = $config->get('clips.audio_media_type');
    $form['audio_settings']['audio_media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal audio media type'),
      '#default_value' => $audio_media_type,
      '#options' => $media_type_options,
    ];
    // Media image field mappings.
    if (!empty($audio_media_type)) {
      $form['audio_settings']['audio_field_mappings'] = [
        '#type' => 'details',
        '#title' => $this->t('Audio field mappings'),
        '#open' => TRUE,
      ];
      $audio_media_fields = array_keys($this
        ->entityFieldManager
        ->getFieldDefinitions('media', $audio_media_type));
      $audio_field_options = ['unused' => 'unused'] +
        array_combine($audio_media_fields, $audio_media_fields);
      $omny_audio_fields = $config->get('clips.audio_field_mappings');
      foreach ($omny_audio_fields as $omny_audio_field => $field_value) {
        $form['audio_settings']['audio_field_mappings'][$omny_audio_field] = [
          '#type' => 'select',
          '#title' => $omny_audio_field,
          '#options' => $audio_field_options,
          '#default_value' => $field_value,
          '#required' => TRUE,
        ];
      }
    }
    else {
      $form['audio_settings']['audio_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save the Drupal audio media type to choose field mappings.',
      ];
    }

    $form['tags'] = [
      '#type' => 'details',
      '#title' => $this->t('Tags vocabulary'),
    ];

    $vocabs = array_keys($this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple());
    $vocabulary_options = array_combine($vocabs, $vocabs) + ['unused' => 'unused'];

    $form['tags']['tags_vocabulary'] = [
      '#type' => 'select',
      '#title' => 'Tags vocabulary (taxonomy)',
      '#description' => $this->t('Select the vocabulary to use for the Tags field.'),
      '#options' => $vocabulary_options,
      '#default_value' => $config->get('clips.tags_vocabulary'),
    ];


    // Clip references.
    $form['clip_references'] = [
      '#type' => 'details',
      '#title' => $this->t('Clip references'),
    ];

    if (!empty($config->get('clips.drupal_clip_content'))) {
      $form['clip_references']['program_reference'] = [
        '#type' => 'select',
        '#title' => 'Program reference field',
        '#options' => $clip_field_options,
        '#default_value' => $config->get('clips.references.program'),
      ];
    }

    // Transcript mappings
    $form['transcripts'] = [
      '#type' => 'details',
      '#title' => $this->t('Transcripts'),
    ];

    if (!empty($config->get('clips.drupal_clip_content'))) {
      $form['transcripts']['transcript'] = [
        '#type' => 'select',
        '#title' => 'Transcript',
        '#options' => $clip_field_options,
        '#default_value' => $config->get('clips.transcripts.transcript'),
      ];
      $form['transcripts']['hash'] = [
        '#type' => 'select',
        '#title' => 'Transcript hash',
        '#options' => $clip_field_options,
        '#default_value' => $config->get('clips.transcripts.hash'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('weta_omny.settings');

    $config->set('clips.drupal_clip_content', $values['drupal_clip_content']);


    // Clip field mappings.
    $omny_clip_fields = $config->get('clips.mappings');
    foreach (array_keys($omny_clip_fields) as $field_name) {
      if (isset($values[$field_name])) {
        $config->set('clips.mappings.' . $field_name, $values[$field_name]);
      }
    }

    // Image settings.
    $config->set('clips.image_media_type', $values['image_media_type']);
    $omny_image_fields = $config->get('clips.image_field_mappings');
    foreach ($omny_image_fields as $field_name => $field_value) {
      if (isset($values[$field_name])) {
        $config->set('clips.image_field_mappings.' . $field_name, $values[$field_name]);
      }
    }

    // Audio settings.
    $config->set('clips.audio_media_type', $values['audio_media_type']);
    $omny_audio_fields = $config->get('clips.audio_field_mappings');
    foreach ($omny_audio_fields as $field_name => $field_value) {
      if (isset($values[$field_name])) {
        $config->set('clips.audio_field_mappings.' . $field_name, $values[$field_name]);
      }
    }

    // Tags vocabulary and fields.
    $config->set('clips.tags_vocabulary', $values['tags_vocabulary']);

    // Program/playlist settings.
    $config->set('clips.references.program', $values['program_reference']);

    // Transcript fields.
    $config->set('clips.transcripts.transcript', $values['transcript']);
    $config->set('clips.transcripts.hash', $values['hash']);

    $config->save();

    parent::submitForm($form, $form_state);
  }

}

<?php

namespace Drupal\weta_omny\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AudioMappingsForm.
 *
 * @ingroup weta_omny
 */
class AudioMappingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new VideoMappingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(ConfigFactory $config_factory,EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): AudioMappingsForm|ConfigFormBase|static {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'weta_omny_audio_mappings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['weta_omny.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('weta_omny.settings');

    $media_types = array_keys($this->entityTypeManager->getStorage('media_type')->loadMultiple());
    $media_type_options = array_combine($media_types, $media_types);
    $audio_media_type = $config->get('audio.audio_media_type');
    $form['audio_settings']['audio_media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal audio media type'),
      '#default_value' => $audio_media_type,
      '#options' => $media_type_options,
    ];

    // Media video field mappings.
    if (!empty($audio_media_type)) {
      $form['audio_settings']['mappings'] = [
        '#type' => 'details',
        '#title' => $this->t('Audio field mappings'),
        '#open' => FALSE,
      ];
      $audio_media_fields = array_keys($this
        ->entityFieldManager
        ->getFieldDefinitions('media', $audio_media_type));
      $audio_field_options = ['unused' => 'unused'] +
        array_combine($audio_media_fields, $audio_media_fields);
      $required_audio_media_fields = [
        'Id',
        'Title',
        'updated_at',
      ];
      if ($omny_audio_fields = $config->get('audio.mappings')) {
        foreach ($omny_audio_fields as $omny_audio_field => $field_value) {
          $required = in_array($omny_audio_field, $required_audio_media_fields);
          $form['audio_settings']['mappings'][$omny_audio_field] = [
            '#type' => 'select',
            '#title' => $omny_audio_field,
            '#options' => $audio_field_options,
            '#required' => $required,
            '#default_value' => $field_value,
          ];
        }
      }
    }
    else {
      $form['audio_settings']['mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save the Drupal audio media type to choose field mappings.',
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('weta_omny.settings');

    if ($omny_audio_fields = $config->get('audio.mappings')) {
      foreach (array_keys($omny_audio_fields) as $field) {
        if (!empty($values[$field])) {
          $config->set('audio.mappings.' . $field, $values[$field]);
        }
      }
    }

    // Video settings.
    $config->set('audio.audio_media_type', $values['audio_media_type']);
    $omny_audio_fields = $config->get('audio.mappings');
    if (!empty($omny_audio_fields)) {
      foreach ($omny_audio_fields as $field_name => $field_value) {
        if (isset($values[$field_name])) {
          $config->set('audio.mappings.' . $field_name, $values[$field_name]);
        }
      }
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}

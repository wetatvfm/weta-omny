<?php

namespace Drupal\weta_omny\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\weta_omny\ApiClient;
use Drupal\weta_omny\ClipManager;
use Drupal\weta_omny\ProgramManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ProgramMappingsForm.
 *
 * @ingroup weta_omny
 */
class ProgramMappingsForm extends ConfigFormBase {

  /**
   * Omny Studio API client.
   *
   * @var \Drupal\weta_omny\ApiClient
   */
  protected ApiClient $apiClient;

  /**
   * Date formatting service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new OmnyStudioSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory service.
   * @param \Drupal\weta_omny\ApiClient $api_client
   *   Omny Studio API client service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    ConfigFactory $config_factory,
    ApiClient $api_client,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    parent::__construct($config_factory);
    $this->apiClient = $api_client;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ConfigFormBase|ProgramMappingsForm|static {
    return new static(
      $container->get('config.factory'),
      $container->get('weta_omny.api_client'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'omnystudio_program_mappings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['weta_omny.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('weta_omny.settings');
    $program_content_type = $config->get('programs.drupal_program_content');


    $program_options = [];
    $programs = $this->apiClient->getPrograms();
    foreach ($programs as $program) {
      $program_options[$program->Id] = $program->Name;
    }
    $program_defaults = [];
    $program_settings = $config->get('programs.program_subscriptions');

    if (!empty($program_settings)) {
      foreach ($program_settings as $setting) {
        $program_defaults[] = $setting['Id'];
      }
    }

    $explanation = 'Within Omny Studio, the "Playlist" aligns most closely to our concept of an Audio Program. Therefore, we will be mapping Omny Playlist data to Audio Program fields.';


    $form['programs_queue'] = [
      '#type' => 'details',
      '#title' => $this->t('Program queue settings'),
      '#open' => TRUE,
    ];

    $form['programs_queue']['info'] = [
      '#type' => 'item',
      '#markup' => $explanation,
    ];

    $form['programs_queue']['programs_queue_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable automated queue building'),
      '#description' => $this->t('Enable incremental updates to local Audio Program nodes from Omny Studio Playlist data.'),
      '#default_value' => $config
        ->get(ProgramManager::getAutoUpdateConfigName()),
      '#return_value' => TRUE,
    ];

    $interval_options = [1, 900, 3600, 10800, 21600, 43200, 86400, 604800];
    $form['programs_queue']['programs_queue_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Program queue builder update interval'),
      '#description' => $this->t('How often to check Omny Studio for
        new or updated playlists to add to the queue. The queue itself
        is processed on every cron run (or by an external cron operation).'),
      '#default_value' => $config
        ->get(ProgramManager::getAutoUpdateIntervalConfigName()),
      '#options' => array_map(
        [$this->dateFormatter, 'formatInterval'],
        array_combine($interval_options, $interval_options)
      ),
      '#states' => [
        'visible' => [
          'input[name="programs_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['programs_queue']['clips_queue_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Clip queue builder update interval'),
      '#description' => $this->t('How often to check Omny Studio for
        new or updated clips to add to the queue. The queue itself
        is processed on every cron run (or by an external cron operation).'),
      '#default_value' => $config
        ->get(ClipManager::getAutoUpdateIntervalConfigName()),
      '#options' => array_map(
        [$this->dateFormatter, 'formatInterval'],
        array_combine($interval_options, $interval_options)
      ),
      '#states' => [
        'visible' => [
          'input[name="programs_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Create an array of all Drupal users.
    $users = $this->entityTypeManager->getStorage('user')->loadMultiple();
    $all_users = [];
    foreach ($users as $user) {
      $all_users[$user->id()] = $user->getDisplayName();
    }
    unset($all_users[0]);
    asort($all_users);
    $form['programs_queue']['queue_user'] = [
      '#type' => 'select',
      '#title' => 'Drupal author of queued content',
      '#default_value' => $config->get('programs.queue_user'),
      '#options' => $all_users,
    ];

    $form['programs_queue']['program_subscriptions'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Omny Program Subscriptions'),
      '#description' => $this->t('Select the Omny Studio programs that should have their associated playlists and clips imported.'),
      '#default_value' => $program_defaults,
      '#options' => $program_options,
    ];

    $form['program_content_type'] = [
      '#type' => 'details',
      '#title' => $this->t('Program content type'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="programs_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $drupal_content_types = array_keys($this->entityTypeManager->getStorage('node_type')->loadMultiple());
    $content_type_options = array_combine($drupal_content_types, $drupal_content_types);
    $form['program_content_type']['drupal_program_content'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal program content type'),
      '#description' => $this->t('Select the Drupal content type to use for Omny Studio program content.'),
      '#default_value' => $config->get('programs.drupal_program_content'),
      '#options' => $content_type_options,
    ];

    $form['program_field_mappings'] = [
      '#type' => 'details',
      '#title' => $this->t('Program field mappings'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="programs_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    if (!empty($config->get('programs.drupal_program_content'))) {
      $required_program_fields = [
        'Id',
        'Name',
        'Slug',
        'DescriptionHtml',
      ];
      $program_fields = array_keys($this->entityFieldManager
        ->getFieldDefinitions('node', $program_content_type));
      $program_field_options = ['unused' => 'unused'] +
        array_combine($program_fields, $program_fields);
      $omny_program_fields = $config->get('programs.mappings');
      foreach (array_keys($omny_program_fields) as $field_name) {
        // Directory Links are a second level deep.
        if ($field_name !== 'DirectoryLinks') {
          $required = in_array($field_name, $required_program_fields) ? TRUE : FALSE;
          $form['program_field_mappings'][$field_name] = [
            '#type' => 'select',
            '#title' => $field_name,
            '#options' => $program_field_options,
            '#required' => $required,
            '#default_value' => $omny_program_fields[$field_name],
          ];
        }
        else {
          // Handle directory links.
          $form['program_field_mappings']['directories'] = [
            '#type' => 'details',
            '#title' => $this->t('Podcast Directories'),
            '#open' => TRUE
          ];
          $directory_links = $omny_program_fields['DirectoryLinks'];
          foreach ($directory_links as $directory_link => $value) {
            $required = in_array($directory_link, $required_program_fields);
            $form['program_field_mappings']['directories'][$directory_link] = [
              '#type' => 'select',
              '#title' => $directory_link,
              '#options' => $program_field_options,
              '#required' => $required,
              '#default_value' => $value,
            ];
          }
        }

      }
    }
    else {
      $form['program_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save Drupal program content type (above) to choose field mappings.',
      ];
    }

    // Image media type configuration.
    $form['image_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Image settings'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="programs_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $media_types = array_keys($this->entityTypeManager->getStorage('media_type')->loadMultiple());
    $media_type_options = array_combine($media_types, $media_types);
    $image_media_type = $config->get('programs.image_media_type');
    $form['image_settings']['image_media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal image media type'),
      '#default_value' => $image_media_type,
      '#options' => $media_type_options,
    ];
    // Media image field mappings.
    if (!empty($image_media_type)) {
      $form['image_settings']['image_field_mappings'] = [
        '#type' => 'details',
        '#title' => $this->t('Image field mappings'),
        '#open' => TRUE,
      ];
      $image_media_fields = array_keys($this
        ->entityFieldManager
        ->getFieldDefinitions('media', $image_media_type));
      $image_field_options = ['unused' => 'unused'] +
        array_combine($image_media_fields, $image_media_fields);
      $omny_image_fields = $config->get('programs.image_field_mappings');
      foreach ($omny_image_fields as $omny_image_field => $field_value) {
        $form['image_settings']['image_field_mappings'][$omny_image_field] = [
          '#type' => 'select',
          '#title' => $omny_image_field,
          '#options' => $image_field_options,
          '#default_value' => $omny_image_fields[$omny_image_field],
          '#required' => TRUE,
        ];
      }
    }
    else {
      $form['image_settings']['image_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save the Drupal image media type to choose field mappings.',
      ];
    }

    $form['vocabularies'] = [
      '#type' => 'details',
      '#title' => $this->t('Vocabulary settings'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="programs_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $vocabs = array_keys($this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple());
    $vocabulary_options = array_combine($vocabs, $vocabs) + ['unused' => 'unused'];

    $form['vocabularies']['podcast_vocabulary'] = [
      '#type' => 'select',
      '#title' => 'Podcast category vocabulary (taxonomy)',
      '#description' => $this->t('Select the vocabulary to use for the podcast category field.'),
      '#options' => $vocabulary_options,
      '#default_value' => $config->get('programs.podcast_vocabulary'),
    ];

    $form['vocabularies']['playlist_vocabulary'] = [
      '#type' => 'select',
      '#title' => 'Playlist vocabulary (taxonomy)',
      '#description' => $this->t('Select the vocabulary to use for the playlist field.'),
      '#options' => $vocabulary_options,
      '#default_value' => $config->get('programs.playlist_vocabulary'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('weta_omny.settings');

    // Program subscription config.
    $program_settings = [];
    $programs = $values['program_subscriptions'];
    foreach ($programs as $program) {
      if ($program) {
        $program_settings[$program]['Id'] = $program;
        $program_settings[$program]['Name'] = $form['programs_queue']['program_subscriptions']['#options'][$program];
      }
    }
    $config->set('programs.program_subscriptions', $program_settings);

    // Content type.
    $config->set('programs.drupal_program_content', $values['drupal_program_content']);

    // Queue settings.
    $config->set(
      ProgramManager::getAutoUpdateConfigName(),
      $values['programs_queue_enable']
    );
    if ($values['programs_queue_enable']) {
      $config->set(
        ProgramManager::getAutoUpdateIntervalConfigName(),
        (int) $values['programs_queue_interval']
      );
      $config->set(
        ClipManager::getAutoUpdateIntervalConfigName(),
        (int) $values['clips_queue_interval']
      );
    }

    $config->set('programs.queue_user', $values['queue_user']);

    // Program field mappings.
    $omny_program_fields = $config->get('programs.mappings');

    foreach (array_keys($omny_program_fields) as $field_name) {
      if ($field_name == 'DirectoryLinks') {
        $directory_links = $config->get('programs.mappings.DirectoryLinks');
        foreach (array_keys($directory_links) as $directory_name) {
          if (isset($values[$directory_name])) {
            $config->set('programs.mappings.DirectoryLinks.' . $directory_name, $values[$directory_name]);
          }
        }
      }
      else {
        if (isset($values[$field_name])) {
          $config->set('programs.mappings.' . $field_name, $values[$field_name]);
        }
      }

    }

    // Image settings.
    $config->set('programs.image_media_type', $values['image_media_type']);
    $omny_image_fields = $config->get('programs.image_field_mappings');
    foreach ($omny_image_fields as $field_name => $field_value) {
      if (isset($values[$field_name])) {
        $config->set('programs.image_field_mappings.' . $field_name, $values[$field_name]);
      }
    }

    // Vocabulary settings.
    $config->set('programs.podcast_vocabulary', $values['podcast_vocabulary']);

    $config->save();

    parent::submitForm($form, $form_state);
  }

}

<?php


namespace Drupal\weta_omny;


use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;

class TranscriptManager {
  use StringTranslationTrait;

  /**
   * Omny Studio Consumer API client.
   *
   * @var \Drupal\weta_omny\ConsumerClient
   */
  protected ConsumerClient $client;


  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The Omny Studio settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Omny Studio logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Queue service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected QueueFactory $queueFactory;

  /**
   * State interface.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * TranscriptManager constructor.
   *
   * @param \Drupal\weta_omny\ConsumerClient $client
   *   Omny Studio API client service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger channel service.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   Queue factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   */
  public function __construct(
    ConsumerClient $client,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    ConfigFactoryInterface $config_factory,
    LoggerChannelInterface $logger,
    QueueFactory $queue_factory,
    StateInterface $state
  ) {
    $this->client = $client;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->config = $config_factory->get('weta_omny.settings');
    $this->logger = $logger;
    $this->queueFactory = $queue_factory;
    $this->state = $state;
  }

  /**
   * @param NodeInterface $node
   *   A clip node.
   * @param bool $force
   *   Whether to force an update.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addOrUpdateTranscript(NodeInterface $node, bool $force = FALSE):void {
    $mappings = $this->config->get('clips.mappings');
    $transcript_mappings = $this->config->get('clips.transcripts');
    $disable_notices = $this->config->get('api.disable_notices');

    $transcript_field = $transcript_mappings['transcript'];
    $transcript_hash_field = $transcript_mappings['hash'];

    // Ensure the fields we need are available and configured.
    if ($transcript_field && $transcript_field != 'unused' && $transcript_hash_field && $transcript_hash_field != 'unused' && $mappings['Id'] != 'unused') {
      $configured = TRUE;
    }
    else {
      $configured = FALSE;
      if (!$disable_notices) {
        $this->logger->notice($this->t('The transcript fields for clips have not been configured.', [
          '@title' => $node->getTitle(),
        ]));
      }
    }


    // Get the GUID from the clip node.
    if ($configured) {
      $guid = $node->get($mappings['Id'])->value;
      if ($guid) {
        // Get the transcript.
        $transcript = $this->processTranscript($guid);
        // If there is no transcript, do nothing.
        if (empty($transcript['text'])) {
          if (!$disable_notices) {
            $this->logger->notice($this->t('There is no transcript available for clip @title.', [
              '@title' => $node->getTitle(),
            ]));
          }
        }
        else {
          // If the transcript has changed or this is a forced update, update it.
          $original_hash = $node->get($transcript_hash_field)->value;
          if ($force || $original_hash !== $transcript['hash']) {
            $node->set($transcript_field, [
              'value' => _filter_autop($transcript['text']),
              'format' => 'basic_html'
            ]);
            $node->set($transcript_hash_field, $transcript['hash']);
            // Do not create a revision.
            $node->setSyncing(TRUE);
            $node->save();
            if (!$disable_notices) {
              $this->logger->notice($this->t('The transcript for clip @title has been updated.', [
                '@title' => $node->getTitle(),
              ]));
            }
          }
        }
      }
    }
  }

  /**
   * @param string $guid
   *   The Omny Studio ID for a clip.
   *
   * @return array
   *   An array containing the transcript text and a computed hash.
   */
  public function processTranscript(string $guid):array {
    $transcript = [];
    $transcript['text'] = $this->client->getTranscriptByClip($guid, 'TextWithTimestamps');
    $transcript['hash'] = hash('md5', serialize($transcript['text']));
    return $transcript;
  }

}

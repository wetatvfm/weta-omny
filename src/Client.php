<?php


namespace Drupal\weta_omny;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use League\Uri\Components\Query;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use stdClass;

/**
 * Omny Studio API Client.
 *
 * @url https://api.omnystudio.com/api-docs/index
 *
 */
class Client {

  /**
   * API endpoint base.
   */
  const API_BASE = 'https://api.omnystudio.com';

  /**
   * Response timeout in seconds.
   */
  const TIMEOUT = 300;

  /**
   * Connection timeout in seconds.
   */
  const CONNECT_TIMEOUT = 60;

  /**
   * Client for handling API requests
   *
   * @var GuzzleClient
   */
  protected GuzzleClient $client;

  /**
   * Client constructor.
   *
   * @param string|null $key
   *   API client key.
   * @param string|null $base_uri
   *   Base API URI.
   * @param array $options
   *   Additional options to pass to Guzzle client.
   */
  public function __construct(
    ?string $key = NULL,
    ?string $base_uri = self::API_BASE,
    array $options = []
  ) {
    $options = [
        'base_uri' => $base_uri,
        'headers' => ['Authorization' => 'OmnyToken ' . $key],
        'timeout' => self::TIMEOUT,
        // Connection timeout in seconds.
        'connect_timeout' => self::CONNECT_TIMEOUT,
      ] + $options;
    $this->client = new GuzzleClient($options);
  }

  /**
   * @param string $method
   *   Request method (e.g. 'get', 'post', 'put', etc.).
   * @param string $endpoint
   *   API endpoint to query.
   * @param array $query
   *   Additional query parameters in the form `param => value`.
   *
   * @return ResponseInterface
   *   Response data from the API.
   *
   */
  public function request(string $method, string $endpoint, array $query = []): ResponseInterface
  {
    try {
      $response = $this->client->request($method, $endpoint, [
        'query' => self::buildQuery($query)
      ]);
    } catch (GuzzleException $e) {
      throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
    }

    switch ($response->getStatusCode()) {
      case 200:
      case 204:
        break;
      case 400:
      case 401:
      case 403:
      case 404:
      case 409:
      case 500:
      default:
        throw new RuntimeException($response->getReasonPhrase(), $response->getStatusCode());
    }

    return $response;
  }

  /**
   * Gets an iterator for paging through API responses.
   *
   * @param string $endpoint
   *   URL to query.
   * @param array $query
   *   Additional query parameters in the form `param => value`.
   * @param bool $headers
   *   Whether to include the headers in the response.
   *
   * @return stdClass
   *   JSON decoded object with response data.
   */
  public function get(string $endpoint, array $query = [], bool $headers =
  FALSE): stdClass {
    $response = $this->request('get', $endpoint, $query);
    $data = json_decode($response->getBody()->getContents());
    if ($headers) {
      $headers_array = $response->getHeaders();
      // Ensure the keys for the headers array are lower case to avoid
      // undefined index errors.
      $headers_array = array_change_key_case($headers_array);
      $data->headers = $headers_array;
    }
    return $data;
  }

  /**
   * Creates a query string from an array of parameters.
   *
   * @param array $parameters
   *   Query parameters keyed to convert to "key=value".
   *
   * @return string
   *   All parameters as a string.
   */
  public static function buildQuery(array $parameters): string
  {
    // Remove any empty parameters.
    $parameters = array_filter($parameters);
    $pairs = [];
    foreach ($parameters as $key => $value) {
      $pairs[] = [$key, $value];
    }
    $query = Query::createFromPairs($pairs);

    return (string) $query;
  }

  /**
   *
   * @param array $query
   *   Additional API query parameters.
   *
   * @return array|null
   *   An array of Program objects returned from the API.
   */
  public function getPrograms(array $query = []): ?array
  {
    $endpoint = '/v0/programs';
    $response = $this->get($endpoint, $query);
    if ($response->Programs) {
      return $response->Programs;
    }
    return NULL;
  }

  /**
   *
   * @param array $query
   *   Additional API query parameters.
   *
   * @return array|null
   *   An array of Clip objects returned from the API.
   */
  public function getClips(array $query = []): ?array
  {
    $endpoint = '/v0/clips';
    $response = $this->get($endpoint, $query);

    if ($response->Clips) {
      return $response->Clips;
    }
    return NULL;
  }

  /**
   * @param string $id
   *   The Program ID.
   * @param array $query
   *   The query.
   *
   * @return array
   *   A paginated array of Clip objects returned from the API.
   */
  public function getClipsByProgram(string $id, array $query = []): array
  {
    $endpoint = '/v0/programs/' . $id . '/clips';
    $response = $this->get($endpoint, $query);
    $clips = $response->Clips;

    // If the Cursor is not null, there are additional pages to process.
    if (!empty($response->Cursor)) {
      $clips = $this->getItemsByPage($response, $endpoint, $query, 'Clips');
    }

    return $clips;
  }

  /**
   * Get all Items from a paginated response.
   *
   * @param object $response
   *   An Omny Studio API response object.
   * @param string $endpoint
   *   The endpoint.
   * @param array $query
   *   The query.
   * @param string $type
   *   The type of item (e.g., Clips or Playlists)
   * @param array $items
   *   Clips already returned.
   *
   * @return array
   *   Returns a combined array of all the requested clips.
   */
  public function getItemsByPage(object $response, string $endpoint, array $query, string $type, array $items = []): array {
    // Populate the clips array with the current response.
    $items = array_merge($items, $response->{$type});

    // If the cursor isn't null, there is another page to add.
    if (!empty($response->Cursor)) {
      $query['cursor'] = $response->Cursor;
      $next_page = $this->get($endpoint, $query, TRUE);
      // If there are enough clips, the API request will be denied due to rate
      // limits. When we're approaching that cut-off, wait for the rate limit
      // timer to reset before continuing.
      if (isset ($response->headers) && $response->headers['x-ratelimit-remaining'][0] == 2) {
        sleep($response->headers['x-ratelimit-reset'][0] + 1);
      }
      $items = $this->getItemsByPage($next_page, $endpoint, $query, $type, $items);
    }

    return $items;
  }

  /**
   *
   * @param string $id
   *   The Clip ID
   *
   * @param array $query
   *   Additional API query parameters.
   *
   * @return stdClass
   *   A single Clip object.
   */
  public function getClip(string $id, array $query = []): stdClass
  {
    $endpoint = '/v0/clips/' . $id;
    return $this->get($endpoint, $query);

  }

  /**
   *
   * @param string $id
   *   The Playlist ID
   *
   * @param array $query
   *   Additional API query parameters.
   *
   * @return stdClass
   *   A single Playlist object.
   */
  public function getPlaylist(string $id, array $query = []): stdClass
  {
    $endpoint = '/v0/playlists/' . $id;
    return $this->get($endpoint, $query);

  }


  /**
   *
   * @param string $id
   *   The Program ID
   *
   * @param array $query
   *   Additional API query parameters.
   *
   * @return array
   *   An array of Playlist objects returned from the API.
   */
  public function getPlaylistsByProgram(string $id, array $query = []): array
  {
    $endpoint = '/v0/programs/' . $id . '/playlists';
    $response = $this->get($endpoint, $query);

    return $response->Playlists;
  }
}

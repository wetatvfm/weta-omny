<?php

namespace Drupal\weta_omny;

use DateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\NodeInterface;
use Exception;
use stdClass;

/**
 * Class ProgramManager.
 *
 * @package Drupal\weta_ommy
 */
class ProgramManager extends ApiContentManagerBase {

  /**
   * State key for the last update DateTime.
   */
  const LAST_UPDATE_KEY = 'weta_omny.programs.last_update';

  /**
   * {@inheritdoc}
   */
  public static function getEntityTypeId(): string {
    return 'node';
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleId(): string {
    return $this->config->get('programs.drupal_program_content');
  }

  /**
   * {@inheritdoc}
   */
  public static function getQueueName(): string {
    return 'weta_omny.queue.programs';
  }

  /**
   * {@inheritdoc}
   */
  public function getLastUpdateTime(): DateTime {
    return $this->state->get(
      self::LAST_UPDATE_KEY,
      new DateTime('@1')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setLastUpdateTime(DateTime $time): void {
    $this->state->set(self::LAST_UPDATE_KEY, $time);
  }

  /**
   * {@inheritdoc}
   */
  public function resetLastUpdateTime(): void {
    $this->state->delete(self::LAST_UPDATE_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public static function getAutoUpdateConfigName(): string {
    return 'weta_omny.queue.autoupdate';
  }

  /**
   * {@inheritdoc}
   */
  public static function getAutoUpdateIntervalConfigName(): string {
    return 'programs.queue.autoupdate_interval';
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function updateQueue(DateTime $since = NULL): bool {
    $dt_start = new DateTime();

    $programs = $this->config->get('programs.program_subscriptions');

    foreach ($programs as $program) {
      $playlists = $this->client->getPlaylistsByProgram($program['Id']);
      foreach ($playlists as $playlist) {
        $this->getQueue()->createItem($playlist);
      }
    }

    $this->setLastUpdateTime($dt_start);

    return TRUE;
  }

  /**
   * Gets an API response for a single Playlist.
   *
   * @param string $id
   *   ID of the Playlist to get.
   *
   * @return object|null
   *   Show item data from the API or NULL.
   */
  public function getPlaylist(string $id): ?stdClass {
    return $this->client->getPlaylist($id);
  }

  /**
   * Get Program nodes with optional properties.
   *
   * @param array $properties
   *   Properties to filter Program nodes.
   *
   * @return \Drupal\node\NodeInterface[]
   *   Program nodes.
   */
  public function getProgramNodes(array $properties = []): array {
    try {
      $definition = $this->entityTypeManager->getDefinition(self::getEntityTypeId());
      $storage = $this->entityTypeManager->getStorage(self::getEntityTypeId());
      $nodes = $storage->loadByProperties([
        $definition->getKey('bundle') => $this->getBundleId(),
      ] + $properties);
    }
    catch (Exception $e) {
      // Let NULL fall through.
      $nodes = [];
    }

    return $nodes;
  }

  /**
   * Gets a Program node by Omny Playlist ID.
   *
   * @param string $id
   *   Omny ID to query with.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Show node with Omny Playlist ID or NULL if none found.
   */
  public function getProgramNodeByOmnyId(string $id): ?NodeInterface {
    $node = NULL;
    if ($omny_id_field = $this->config->get('programs.mappings.Id')) {
      $nodes = $this->getProgramNodes([$omny_id_field => $id]);
      if (!empty($nodes)) {
        $node = reset($nodes);
        if (count($nodes) > 1) {
          $this->logger->error('Multiple nodes found for Omny Playlist ID {id}. Node IDs found: {nid_list}. Using node {nid}.', [
              'id' => $id,
              'nid_list' => implode(', ', array_keys($nodes)),
              'nid' => $node->id(),
            ]);
        }
      }
    }
    else {
      $this->logger->error('Mapping the Program Omny ID field is required.');
    }
    return $node;
  }

  /**
   * Gets a Program node by slug.
   *
   * @param string $slug
   *   Slug to query with.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Program node with the slug or NULL if none found.
   */
  public function getProgramNodeBySlug(string $slug): ?NodeInterface {
    $nodes = $this->getProgramNodes(['field_omny_slug' => $slug]);

    $node = NULL;
    if (!empty($nodes)) {
      $node = reset($nodes);
    }

    return $node;
  }

  /**
   * Adds or updates an Audio Program node based on API data. We are treating
   * Playlists as Programs, so this is expecting to receive a Playlist object.
   *
   * @param object $item
   *   An API response object for the Playlist.
   * @param bool $force
   *   Whether to "force" the update. If FALSE (default) and a
   *   current audio program content node is found with a matching
   *   "update_hash" value, the existing node will not be updated.
   *   If TRUE, "update_hash" is ignored.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function addOrUpdateAudioProgram(object $item, bool $force = FALSE): void {

    $mappings = $this->config->get('programs.mappings');
    $disable_notices = $this->config->get('api.disable_notices');
    $node = $this->getOrCreateNode($item->Id, $this->getBundleId(), 'programs');


    // Create a hash of the API item. This does not need to be secure.
    $hash = hash('md5', serialize($item));
    $item->update_hash = $hash;

    $operation = $this->getOperation($item, $node, $mappings, $force);

    // Do not proceed if mappings are missing.
    if ($operation == 'configured') {
      $this->logger->error($this->t('Please map the title, id, and update hash fields for programs.'));
      return;
    }

    // Do not consider ignored programs.
    if ($operation == 'ignored') {
      if (!$disable_notices) {
        $this->logger->notice($this->t('The program @id was ignored due to a hook_weta_omny_operation_alter() implementation.', [
          '@id' => $item->Id,
        ]));
      }
      return;
    }

    // We need to save the node before creating the clips because programs
    // reference clips and clips reference programs. Therefore, we also
    // have to set the title of the program. Additionally, we need the
    // Omny ID because addOrUpdateClipContent() looks up nodes by Omny ID.
    if (empty($item->Title)) {
      return;
    }
    $node->setTitle($item->Title);
    if (!empty($item->Id) && $mappings['Id'] != 'unused') {
      $node->set($mappings['Id'], $item->Id);
    }
    // Do not create a revision.
    $node->setSyncing(TRUE);
    $node->save();

    if ($operation == 'skipped') {
      if (!$disable_notices) {
        $this->logger->notice($this->t('The Audio Program @title has not changed', [
          '@title' => $node->getTitle(),
        ]));
      }
      return;
    }

    $special_handling = [
      'ArtworkUrl',
      'Categories',
      'DirectoryLinks',
      'NumberOfClips',
      'DescriptionHtml'
    ];

    // Loop through the item properties (except those listed above)
    // and add them to the node based on their mappings if set.
    foreach ($item as $attribute => $value) {
      if (!in_array($attribute, $special_handling)) {
        if (!empty($item->{$attribute}) && $mappings[$attribute] != 'unused') {
          $node->set($mappings[$attribute], $value);
        }
      }

      if ($attribute == 'DescriptionHtml' && !empty($item->{$attribute}) && $mappings[$attribute] != 'unused') {
        $node->set($mappings['DescriptionHtml'], [
          'value' => $value,
          'format' => 'basic_html'
        ]);
      }
    }



    $vocabulary = $this->config->get('programs.podcast_vocabulary');

    // Add the categories, if mapped.
    if (!empty($item->Categories) && $categories_field = $mappings['Categories']) {
      if ($categories_field != 'unused') {
        // Check to see if the node already has categories.
        $original_categories = [];
        $existing = $node->get($categories_field)->referencedEntities();

        // Populate the array for comparison.
        if ($existing) {
          foreach ($existing as $existing_category) {
            $original_categories[] = $existing_category->getName();
          }

          // If categories have changed, remove them and empty the array.
          if (array_values($item->Categories) !== array_values($original_categories)) {
            unset($node->{$categories_field});
            $original_categories = [];
          }
        }

        // If there are no existing categories, or if they have changed,
        // add or update them.
        if (empty($original_categories)) {
          foreach ($item->Categories as $i => $category) {
            // Because this is a multi-value field, we can set the first
            // one, but then need to append the rest.
            if ($i == 0) {
              $node->set($categories_field, $this->getOrAddTerm($category, $vocabulary));
            } else {
              $node->get($categories_field)->appendItem($this->getOrAddTerm($category, $vocabulary));
            }
          }
        }
      }
    }

    // Process images.
    if (!empty($item->ArtworkUrl) && $mappings['ArtworkUrl'] != 'unused') {
        // Check to see if this item has an image already.
        $media_image = NULL;
        $image_field = $mappings['ArtworkUrl'];
        if ($node->get($image_field)->target_id) {
          // Get the existing image entity.
          $image_id = $node->get($image_field)->target_id;
          $media_storage = $this->entityTypeManager->getStorage('media');
          $media_image = $media_storage->load($image_id);
        }
        if ($media_id = $this->addOrUpdateMediaImage(
          $item->ArtworkUrl,
          $item->Title,
          'program',
          $media_image
        )) {
          $node->set($image_field, ['target_id' => $media_id]);
        }
      }

    // Process Directory Links
    if (!empty($item->DirectoryLinks)) {
      $directory_mappings = $this->config->get('programs.mappings.DirectoryLinks');
      foreach ($item->DirectoryLinks as $attribute => $value) {
        if (!empty($item->DirectoryLinks->{$attribute}) && $directory_mappings[$attribute] != 'unused') {
          $node->set($directory_mappings[$attribute], $value);
        }
      }
    }

    // Set updated at field to the current time.
    $updated_at = self::getLatestUpdatedAt();
    $node->set(
      $mappings['updated_at'],
      $updated_at->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
    );

    // Do not create a revision.
    $node->setSyncing(TRUE);
    $node->save();

    if (!empty($node->getTitle()) & !empty($operation)) {
      if (!$disable_notices) {
        $this->logger->notice($this->t('Program @title has been @operation', [
          '@title' => $node->getTitle(),
          '@operation' => $operation,
        ]));
      }
    }
  }
 }

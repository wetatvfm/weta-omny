<?php

namespace Drupal\weta_omny;

use DateTime;
use DateTimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\State\StateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\NodeInterface;
use Exception;
use stdClass;

/**
 * Class ClipManager.
 *
 * @package Drupal\weta_ommy
 */
class ClipManager extends ApiContentManagerBase {

  /**
   * State key for the last update DateTime.
   */
  const LAST_UPDATE_KEY = 'weta_omny.clips.last_update';

  /**
   * @var \Drupal\weta_omny\TranscriptManager
   *   The Omny Studio transcript manager.
   */
  private TranscriptManager $transcriptManager;

  /**
   * ApiContentManagerBase constructor.
   *
   * @param \Drupal\weta_omny\ApiClient $client
   *   Omny Studio API client service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger channel service.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   Queue factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The filesystem service.
   * @param \Drupal\weta_omny\TranscriptManager $transcript_manager
   */
  public function __construct(
    ApiClient $client,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    ConfigFactoryInterface $config_factory,
    LoggerChannelInterface $logger,
    QueueFactory $queue_factory,
    StateInterface $state,
    FileSystemInterface $file_system,
    TranscriptManager $transcript_manager
  ) {
    parent::__construct($client, $entity_type_manager, $module_handler, $config_factory, $logger, $queue_factory, $state, $file_system);
    $this->transcriptManager = $transcript_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getEntityTypeId(): string {
    return 'node';
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleId(): string {
    return $this->config->get('clips.drupal_clip_content');
  }

  /**
   * {@inheritdoc}
   */
  public static function getQueueName(): string {
    return 'weta_omny.queue.clips';
  }

  /**
   * {@inheritdoc}
   */
  public function getLastUpdateTime(): DateTime {
    return $this->state->get(
      self::LAST_UPDATE_KEY,
      new DateTime('@1')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setLastUpdateTime(DateTime $time): void {
    $this->state->set(self::LAST_UPDATE_KEY, $time);
  }

  /**
   * {@inheritdoc}
   */
  public function resetLastUpdateTime(): void {
    $this->state->delete(self::LAST_UPDATE_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public static function getAutoUpdateConfigName(): string {
    return 'weta_omny.queue.autoupdate';
  }

  /**
   * {@inheritdoc}
   */
  public static function getAutoUpdateIntervalConfigName(): string {
    return 'clips.queue.autoupdate_interval';
  }



  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function updateQueue(DateTime $since = NULL): bool {
    $dt_start = new DateTime();

    // Get all clips for subscribed programs.
    $programs = $this->config->get('programs.program_subscriptions');
    foreach ($programs as $program) {
      $clips = $this->client->getClipsByProgram($program['Id']);
      foreach ($clips as $clip) {
        $this->getQueue()->createItem($clip);
      }
    }

    $this->setLastUpdateTime($dt_start);

    return TRUE;
  }

  /**
   * Force all clips for all subscribed programs to update.
   */
  public function forceUpdateAllClips(): void {

    // Get all clips for subscribed programs.
    $programs = $this->config->get('programs.program_subscriptions');
    foreach ($programs as $program) {
      $clips = $this->client->getClipsByProgram($program['Id']);
      foreach ($clips as $clip) {
        $this->addOrUpdateClip($clip, TRUE);
      }
    }
  }

  /**
   * Gets an API response for a single Clip.
   *
   * @param string $guid
   *   ID of the Clip to get.
   *
   * @return object|null
   *   Show item data from the API or NULL.
   */
  public function getClip(string $guid): ?stdClass {
    return $this->client->getClip($guid);
  }

  /**
   * Get Clip nodes with optional properties.
   *
   * @param array $properties
   *   Properties to filter Clip nodes.
   *
   * @return \Drupal\node\NodeInterface[]
   *   Shows nodes.
   */
  public function getClipNodes(array $properties = []): array {
    try {
      $definition = $this->entityTypeManager->getDefinition(self::getEntityTypeId());
      $storage = $this->entityTypeManager->getStorage(self::getEntityTypeId());
      $nodes = $storage->loadByProperties([
        $definition->getKey('bundle') => $this->getBundleId(),
      ] + $properties);
    }
    catch (Exception) {
      // Let NULL fall through.
      $nodes = [];
    }

    return $nodes;
  }

  /**
   * Gets a Clip node by Omny ID.
   *
   * @param string $id
   *   Omny ID to query with.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Clip node with Omny ID or NULL if none found.
   */
  public function getClipNodeByOmnyId(string $id): ?NodeInterface {
    $node = NULL;
    if ($omny_id_field = $this->config->get('clips.mappings.Id')) {
      $nodes = $this->getClipNodes([$omny_id_field => $id]);
      if (!empty($nodes)) {
        $node = reset($nodes);
        if (count($nodes) > 1) {
          $this->logger->error('Multiple nodes found for Omny ID {id}. Node IDs found: {nid_list}. Using node {nid}.', [
              'id' => $id,
              'nid_list' => implode(', ', array_keys($nodes)),
              'nid' => $node->id(),
            ]);
        }
      }
    }
    else {
      $this->logger->error('Mapping the clip Omny ID field is required.');
    }
    return $node;
  }

  /**
   * Gets a Clip node by External ID.
   *
   * @param string $id
   *   External ID to query with. This should be the Node ID.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Clip node with the External ID or NULL if none found.
   */
  public function getClipNodeByExternalId(string $id): ?NodeInterface {
    try {
      /** @var \Drupal\node\NodeInterface $node */
      $node = $this->entityTypeManager->getStorage('node')->load($id);
    }
    catch (Exception) {
      // Let NULL fall through.
      $node = NULL;
    }

    if (is_object($node) && $node->getType() !== 'audio_program_episode') {
      $node = NULL;
    }

    return $node;
  }

  /**
   * Gets a Clip node by slug.
   *
   * @param string $slug
   *   Slug to query with.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Clip node with the slug or NULL if none found.
   */
  public function getClipNodeBySlug(string $slug): ?NodeInterface {
    $nodes = $this->getClipNodes(['field_omny_slug' => $slug]);

    $node = NULL;
    if (!empty($nodes)) {
      $node = reset($nodes);
    }

    return $node;
  }

  /**
   * Adds or updates a Clip node based on API data.
   *
   * @param object $item
   *   An API response object for the Clip.
   * @param bool $force
   *   Whether to "force" the update. If FALSE (default) and a
   *   current clip content node is found with a matching
   *   "update_hash" value, the existing node will not be updated.
   *   If TRUE, "update_hash" is ignored.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function addOrUpdateClip(object $item, bool $force = FALSE): void {
    $mappings = $this->config->get('clips.mappings');
    $disable_notices = $this->config->get('api.disable_notices');
    // Check to see if there is a node id that matches the external id.
    $node = NULL;
    if (!empty($item->ExternalId)) {
      $node = $this->getClipNodeByExternalId($item->ExternalId);
    }
    // If not, get or create a node using the Omny ID.
    if (empty($node)) {
      $node = $this->getOrCreateNode($item->Id, $this->getBundleId(), 'clips');
    }


    // Create a hash of the API item. This does not need to be secure.
    $hash = hash('md5', serialize($item));
    $item->update_hash = $hash;

    $operation = $this->getOperation($item, $node, $mappings, $force);

    // Do not proceed if mappings are missing.
    if ($operation == 'configured') {
      $this->logger->error($this->t('Please map the title, id, and update hash fields for clips.'));
      return;
    }
    // Do not consider clips for ignored programs.
    if ($operation == 'ignored') {
      if (!$disable_notices) {
        $this->logger->notice($this->t('The clip @id was ignored due to a hook_weta_omny_operation_alter() implementation.', [
          '@id' => $item->Id,
        ]));
      }
      return;
    }

    // We need to save the node before creating the clips because programs
    // reference clips and clips reference programs. Therefore, we also
    // have to set the title of the clip. Additionally, we need the
    // Omny ID because addOrUpdateClipContent() looks up nodes by Omny ID.
    if (empty($item->Title)) {
      return;
    }
    $node->setTitle($item->Title);
    if (!empty($item->Id) && $mappings['Id'] != 'unused') {
      $node->set($mappings['Id'], $item->Id);
    }

    // Do not create a revision.
    $node->setSyncing(TRUE);
    $node->save();


    if ($operation == 'skipped') {
      // The clip is unaware of the transcript, so we need to check for updates even if the clip is unchanged.
      $this->transcriptManager->addOrUpdateTranscript($node, $force);

      if (!$disable_notices) {
        $this->logger->notice($this->t('The clip @title has not changed', [
          '@title' => $node->getTitle(),
        ]));
      }
      return;
    }

    $specialHandling = [
      'Id',
      'Title',
      'ImageUrl',
      'Tags',
      'PlaylistIds',
      'Chapters', //TODO Later if necessary
      'PublishState',
      'PublishedUtc',
      'ExternalId', //TODO on push?
      'DescriptionHtml'
      ];

    // Loop through the item properties (except those listed above)
    // and add them to the node based on their mappings if set.
    foreach ($item as $attribute => $value) {
      if (!in_array($attribute, $specialHandling)) {
        if (!empty($item->{$attribute}) && isset($mappings[$attribute]) && $mappings[$attribute] != 'unused') {
          $node->set($mappings[$attribute], $value);
        }
      }

      if ($attribute == 'DescriptionHtml' && !empty($item->{$attribute}) && $mappings[$attribute] != 'unused') {
        $node->set($mappings['DescriptionHtml'], [
          'value' => $value,
          'format' => 'basic_html'
        ]);
      }
    }

    // Add the tags, if mapped.
    if (!empty($item->Tags) && $tags_field = $mappings['Tags']) {
      if ($tags_field != 'unused') {
        // Check to see if the node already has tags.
        $original_tags = [];
        $existing = $node->get($tags_field)->referencedEntities();

        // Populate the array for comparison.
        if ($existing) {
          foreach ($existing as $existing_tag) {
            $original_tags[] = $existing_tag->getName();
          }

          // If tags have changed, remove them and empty the array.
          if (array_values($item->Tags) !== array_values($original_tags)) {
            unset($node->{$tags_field});
            $original_tags = [];
          }
        }

        // If there are no existing tags, or if they have changed,
        // add or update them.
        if (empty($original_tags)) {
          $vocabulary = $this->config->get('clips.tags_vocabulary');
          foreach ($item->Tags as $i => $tag) {
            $tag_term = $this->getOrAddTerm($tag, $vocabulary);
            // Because this is a multi-value field, we can set the first
            // one, but then need to append the rest.
            if ($i == 0) {
              $node->set($tags_field, ['target_id' => $tag_term->id()]);
            } else {
              $node->get($tags_field)->appendItem(['target_id' => $tag_term->id()]);
            }
          }
        }
      }
    }

    // Add the Published UTC, if mapped.
    if (!empty($item->PublishedUtc) && $published_date_field = $mappings['PublishedUtc']) {
      if ($published_date_field != 'unused') {
        // Convert date string from API to a date object.
        $published_date = DateTime::createFromFormat('Y-m-d\TH:i:s.uP', $item->PublishedUtc);
        // Legacy clips have a different date format.
        if (!$published_date) {
          $published_date =
            DateTime::createFromFormat(DateTimeInterface::W3C, $item->PublishedUtc);
        }
        if ($published_date) {
          $node->set($mappings['PublishedUtc'], $published_date->format(DateTimeItemInterface::DATE_STORAGE_FORMAT));
        }

      }
    }

    // Add the playlist reference, if configured.
    $playlist_ref_field = $this->config->get('clips.references.program');
    $playlist_guids = $item->PlaylistIds;
    $program_bundle = $this->config->get('programs.drupal_program_content');
    $program_id_field = $this->config->get('programs.mappings.Id');
    if (!empty($playlist_guids) && !empty($program_bundle) && $playlist_ref_field != 'unused') {

      // Check to see if the clip already has playlists.
      $original_playlists = [];
      $existing = $node->get($playlist_ref_field)->referencedEntities();

      // Populate the array for comparison.
      if ($existing) {
        foreach ($existing as $existing_playlist) {
          $original_playlists[] = $existing_playlist->get($program_id_field);
        }

        // If playlists have changed, remove them and empty the array.
        if (array_values($item->PlaylistIds) !== array_values($original_playlists)) {
          unset($node->{$playlist_ref_field});
          $original_playlists = [];
        }
      }

      // If there are no existing playlists, or if they have changed,
      // add or update them.
      if (empty($original_playlists)) {
        foreach ($playlist_guids as $i => $playlist_guid) {
          $program = $this->getNodeByGuid($playlist_guid, $program_bundle, 'programs');

          // If the playlist hasn't been imported yet, do so.
          if (!$program) {
            /** @var \Drupal\weta_omny\ProgramManager $programManager */
            $programManager = \Drupal::service('weta_omny.program_manager');
            $playlist_item = $programManager->getPlaylist($playlist_guid);
            $programManager->addOrUpdateAudioProgram($playlist_item);
            $program = $this->getNodeByGuid($playlist_guid, $program_bundle,
              'programs');
          }

          // Because this is a multi-value field, we can set the first
          // one, but then need to append the rest.
          if ($i == 0) {
            $node->set($playlist_ref_field, ['target_id' => $program->id()]);
          }
          else {
            $node->get($playlist_ref_field)->appendItem(['target_id' => $program->id()]);
          }
        }
      }
    }

    // Process audio
    if (!empty($item->AudioUrl) && $mappings['AudioUrl'] != 'unused') {
      $audio_field = $mappings['AudioUrl'];
      if ($media_audio = $this->addOrUpdateAudio($item, $force)) {
        $node->set($audio_field, $media_audio);
      }

    }


    // Process images.
    if (!empty($item->ImageUrl) && $mappings['ImageUrl'] != 'unused') {

        // Check to see if this item has an image already.
        $media_image = NULL;
        $image_field = $mappings['ImageUrl'];

        if ($node->get($image_field)->target_id) {
          // Get the existing image entity.
          $image_id = $node->get($image_field)->target_id;
          $media_storage = $this->entityTypeManager->getStorage('media');
          $media_image = $media_storage->load($image_id);
        }
        if ($media_id = $this->addOrUpdateMediaImage(
          $item->ImageUrl,
          $item->Title,
          'clip',
          $media_image
        )) {
          $node->set($image_field, ['target_id' => $media_id]);
        }
      }

    // Set updated at field to the current time.
    $updated_at = self::getLatestUpdatedAt();
    $node->set(
      $mappings['updated_at'],
      $updated_at->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
    );

    // Set published state if configured.
    if (!empty($item->PublishState) && $published_field = $mappings['PublishState']) {
      if ($published_field != 'unused') {
        if ($item->PublishState == 'Published') {
          $node->setPublished();
        }
        else {
          $node->setUnpublished();
        }
      }
    }

    // Do not create a revision.
    $node->setSyncing(TRUE);
    $node->save();

    // Check for an associated transcript and add or update it.
    $this->transcriptManager->addOrUpdateTranscript($node, $force);

    if (!empty($node->getTitle()) & !empty($operation)) {
      if (!$disable_notices) {
        $this->logger->notice($this->t('Clip @title has been @operation', [
          '@title' => $node->getTitle(),
          '@operation' => $operation,
        ]));
      }
    }

  }

  /**
   * Adds or updates Drupal audio media data with Omny Studio API data.
   *
   * @param object $item
   *   Omny API data for a Clip.
   *
   * @param bool $force
   *   Whether to force an update even if the content hasn't been updated.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The Drupal audio media entity.
   */
  public function addOrUpdateAudio(object $item, bool $force = FALSE): ?EntityInterface {

    // Skip if there is no audio.
    if (empty($item->AudioUrl)) {
      return NULL;
    }

    // Check for required configuration.
    $audio_media_type = $this->config->get('audio.audio_media_type');
    if (empty($audio_media_type)) {
      $this->logger->error('Please configure the audio media type.');
      return NULL;
    }
    $mappings = $this->config->get('audio.mappings');
    $disable_notices = $this->config->get('api.disable_notices');

    $media_storage = $this->entityTypeManager->getStorage('media');
    $id_field = $mappings['Id'];
    $title_field = $mappings['Title'];


    // Check to see if the audio entity already exists in Drupal.
    if ($media_audio = $media_storage->loadByProperties([
      $id_field => $item->Id,
    ])) {
      if (count($media_audio) > 1) {
        $this->logger->error(
          $this->t('More than one audio item with the @id (@title) exists. Please delete duplicate audio items.', [
            '@id' => $item->Id,
            '@title' => $item->Title,
          ]));
        return NULL;
      }
      $media_audio = reset($media_audio);
    }
    // Otherwise, create a new audio media entity.
    else {
      $media_audio = $media_storage->create([
        $title_field => $item->Title,
        'bundle' => $audio_media_type,
        'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      ]);
      $media_audio->enforceIsNew();
    }
    $operation = $this->getOperation($item, $media_audio, $mappings, $force);

    if ($operation == 'configured') {
      $this->logger->error($this->t('Please map the title, id, and update hash fields for audio items.'));
      return NULL;
    }

    if ($operation == 'skipped') {
      if (!$disable_notices) {
        $this->logger->notice($this->t('The asset @title has not changed', [
          '@title' => $media_audio->getName(),
        ]));
      }
      return $media_audio;
    }
    if ($operation == 'ignored') {
      if (!$disable_notices) {
        $this->logger->notice($this->t('The asset @id was ignored due to a hook_weta_omny_operation_alter() implementation.', [
          '@id' => $item->Id,
        ]));
      }
      return NULL;
    }

    // Omny Clip ID.
    $media_audio->set($id_field, $item->Id);

    $special_handling = ['Id', 'Title', 'DurationSeconds', 'PublishState'];

    // Loop through the item properties (except those listed above)
    // and add them to the audio entity based on their mappings if set.
    foreach ($item as $attribute => $value) {
      if (!in_array($attribute, $special_handling)) {
        if (!empty($item->{$attribute}) && isset($mappings[$attribute]) && $mappings[$attribute] != 'unused') {
          $media_audio->set($mappings[$attribute], $value);
        }
      }
    }

    // Set published state if configured.
    if (!empty($item->PublishState) && $published_field = $mappings['PublishState']) {
      if ($published_field != 'unused') {
        if ($item->PublishState == 'Published') {
          $media_audio->setPublished();
        }
        else {
          $media_audio->setUnpublished();
        }
      }
    }

    // Set duration if configured. Round to nearest second.
    if (!empty($item->DurationSeconds) && $duration_field = $mappings['DurationSeconds']) {
      if ($duration_field != 'unused') {
        $media_audio->set($duration_field, round($item->DurationSeconds));
      }
    }

    if ($uid = $this->config->get('programs.queue_user')) {
      $media_audio->setOwnerId($uid);
    }
    if (!$disable_notices) {
      $this->logger->notice($this->t('The Omny Remote Audio @title has been @operation', [
        '@title' => $media_audio->getName(),
        '@operation' => $operation,
      ]));
    }

    $media_audio->save();

    return $media_audio;

  }
}

<?php

namespace Drupal\weta_omny;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use GuzzleHttp\Exception\RequestException;

/**
 * Class OmnyStudioApi.
 */
class ApiClient extends Client {

  /**
   * Config key for the API key.
   */
  const CONFIG_KEY = 'api.key';


  /**
   * Config key for the API base endpoint.
   */
  const CONFIG_BASE_URI = 'api.base_uri';

  /**
   * API key.
   *
   * @var string
   */
  private mixed $key;


  /**
   * API base endpoint.
   *
   * @var string
   */
  public mixed $baseUri;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The Immutable Config for the Omny API settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * The client for Omny Studio.
   *
   * @var \GuzzleHttp\Client
   */
  protected \GuzzleHttp\Client $client;

  /**
   * ApiClient constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Drupal's config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->config = $configFactory->get('weta_omny.settings');
    $this->key = $this->config->get(self::CONFIG_KEY);
    $this->baseUri = $this->config->get(self::CONFIG_BASE_URI);

    parent::__construct($this->key, $this->baseUri);
  }

  /**
   * Gets the API client key.
   *
   * @return string
   *   Omny Studio API key setting.
   */
  public function getApiKey(): string {
    return $this->key;
  }


  /**
   * Gets the API client base endpoint.
   *
   * @return string
   *   Omny Studio API endpoint setting.
   */
  public function getApiEndPoint(): string {
    return $this->baseUri;
  }

  /**
   * Indicates if the API client is configured.
   *
   * @return bool
   *   TRUE if the key, secret, and endpoint are set. FALSE otherwise.
   */
  public function isConfigured(): bool {
    return (
      !empty($this->getApiKey()) &&
      !empty($this->getApiEndPoint())
    );
  }


  /**
   * Sends a test query to the API and returns an error or "OK".
   *
   * @return string
   *   An error string or "OK" (indicating that the connection is good).
   */
  public function testConnection(): string {
    $endpoint = '/v0/test/ping';
    $result = 'OK';

    try {
      $this->request('get', $endpoint);
    } catch (RequestException $e) {
      $error = json_decode($e->getMessage());
      if (isset($error->detail)) {
        $result = $error->detail;
      }
      else {
        $result = (string) $error;
      }
    }

    return $result;
  }
}

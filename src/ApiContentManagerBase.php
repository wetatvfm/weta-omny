<?php

namespace Drupal\weta_omny;

use DateTime;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\file\FileInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\pathauto\AliasCleaner;
use Drupal\taxonomy\Entity\Term;
use Exception;

/**
 * Class ApiContentManagerBase.
 */
abstract class ApiContentManagerBase implements ApiContentManagerInterface {
  use StringTranslationTrait;

  /**
   * Omny Studio API client.
   *
   * @var \Drupal\weta_omny\ApiClient

   */
  protected ApiClient $client;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The Omny Studio settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected \Drupal\Core\Config\ImmutableConfig $config;

  /**
   * Omny Studio logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Queue service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected QueueFactory $queueFactory;

  /**
   * State interface.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The alias cleaner service.
   *
   * @var \Drupal\pathauto\AliasCleaner
   */
  protected AliasCleaner $aliasCleaner;

  /**
   * ApiContentManagerBase constructor.
   *
   * @param \Drupal\weta_omny\ApiClient $client
   *   Omny Studio API client service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger channel service.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   Queue factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The filesystem service.
   */
  public function __construct(
    ApiClient $client,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    ConfigFactoryInterface $config_factory,
    LoggerChannelInterface $logger,
    QueueFactory $queue_factory,
    StateInterface $state,
    FileSystemInterface $file_system
  ) {
    $this->client = $client;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->config = $config_factory->get('weta_omny.settings');
    $this->logger = $logger;
    $this->queueFactory = $queue_factory;
    $this->state = $state;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueue(): QueueInterface {
    return $this->queueFactory->get($this->getQueueName());
  }

  /**
   * {@inheritdoc}
   */
  public function getApiClient(): ApiClient {
    return $this->client;
  }

  /**
   * Gets or creates a node based on API data.
   *
   * @param string $guid
   *   Omny Studio ID of the object to get a Node for.
   * @param string $bundle
   *   The bundle type of the item being retrieved.
   * @param string $resource
   *   The kind of resource being retrieved.
   *
   * @return \Drupal\node\NodeInterface
   *   The node to use.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getOrCreateNode(string $guid, string $bundle, string $resource): NodeInterface {
    $definition = $this->entityTypeManager->getDefinition($this->getEntityTypeId());
    $node = $this->getNodeByGuid($guid, $bundle, $resource);

    if (empty($node)) {
      $node = Node::create([
        $definition->getKey('bundle') => $bundle,
      ]);
      if ($uid = $this->config->get('programs.queue_user')) {
        $node->setOwnerId($uid);
      }
      $node->enforceIsNew();
    }

    return $node;
  }

  /**
   * Attempts to get Drupal guid field for a resource.
   *
   * @param string $resource
   *   The kind of resource being retrieved.
   *
   * @return string|null
   *   The Drupal ID field of the Omny Studio resource or NULL.
   */
  public function getNodeGuidField(string $resource): ?string {
    return $this->config->get($resource . '.mappings.Id');
  }

  /**
   * Attempts to get a local node by an Omny Studio ID.
   *
   * @param string $guid
   *   Omny Studio GUID of the object to get a Node for.
   * @param string $bundle
   *   The bundle type of the item being retrieved.
   * @param string $resource
   *   The kind of resource being retrieved.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Related node or NULL if none found.
   */
  public function getNodeByGuid(string $guid, string $bundle, string $resource): ?NodeInterface {
    $guid_field = $this->getNodeGuidField($resource);
    try {
      $definition = $this->entityTypeManager->getDefinition($this->getEntityTypeId());
      $storage = $this->entityTypeManager->getStorage($this->getEntityTypeId());
      $nodes = $storage->loadByProperties([
        $definition->getKey('bundle') => $bundle,
        $guid_field => $guid,
      ]);
    }
    catch (Exception $e) {
      // Let NULL fall through.
      $nodes = [];
    }
    $node = NULL;
    if (!empty($nodes)) {
      $node = reset($nodes);
      if (count($nodes) > 1) {
        $this->logger->error('Multiple nodes found for Omny Studio ID {id}. Node IDs found: {nid_list}. Updating node {nid}.', [
            'id' => $guid,
            'nid_list' => implode(', ', array_keys($nodes)),
            'nid' => $node->id(),
          ]);
      }
    }

    return $node;
  }

  /**
   * Returns the ID field from a node or NULL if empty.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node to evaluate.
   *
   * @return string|null
   *   ID field value or NULL.
   */
  public static function getNodeGuid(NodeInterface $node): ?string {
    try {
      /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
      $config_factory = \Drupal::service('config.factory');
      $config = $config_factory->get('weta_omny.settings');
      $id_field = $config->get('program.mappings.id');
      $guid = NULL;
      if ($node->hasField($id_field)) {
        $guid = $node->get($id_field)->value;
      }
      return $guid;
    }
    catch (Exception $e) {
      return NULL;
    }
  }

  /**
   * Indicates if a node has a non-empty ID value.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node to evaluate.
   *
   * @return bool
   *   TRUE if the node has a non-empty ID field, FALSE otherwise.
   */
  public static function nodeHasGuid(NodeInterface $node): bool {
    return !empty(self::getNodeGuid($node));
  }

  /**
   * Gets latest `updated_at` field from the request time.
   *
   * @return \DateTime|null
   *   Latest `updated_at` field value or NULL if DateTime create fails.
   */
  public static function getLatestUpdatedAt(): ?DateTime {
    $request_time = \Drupal::time()->getRequestTime();
    return new DateTime("@{$request_time}");
  }

  /**
   * Creates an image media item based on the configured field values.
   *
   * @param string $image
   *   Image URL from Omny Studio, including profile, image (URL), and updated_at.
   * @param string $image_title
   *   A title for the image.
   * @param string $type
   *   The resource type, such as "program" or "clip".
   * @param object|null $media_image
   *   The existing image, if updating.
   *
   * @return string|null
   *   A Drupal media image id or null.
   */
  protected function addOrUpdateMediaImage(string $image, string $image_title, string $type, ?object $media_image): ?string {

    // Get required configuration.
    $image_media_type = $this->config->get($type . 's.image_media_type');
    if (empty($image_media_type)) {
      $this->logger->error('Please configure the media image type used on ' . $type . 's.');
      return NULL;
    }
    $map_name = 'ArtworkUrl';
    if ($type == 'clip') {
      $map_name = 'ImageUrl';
    }
    $image_field = $this->config->get($type . 's.mappings.' . $map_name);
    if (empty($image_field)) {
      $this->logger->error('Please configure the ' . $type . ' image field.');
      return NULL;
    }
    $image_field_mappings = $this->config->get($type . 's.image_field_mappings');
    $image_title_field = $image_field_mappings['image_title'];
    $image_image_field = $image_field_mappings['image_field'];
    if ($image_title_field == 'unused' || $image_image_field == 'unused') {
      $this->logger->error('Please configure both fields for ' . $type . ' images.');
      return NULL;
    }

    // Check to see if a media image already exists in Drupal with that name.
    $media_storage = $this->entityTypeManager->getStorage('media');

    if ($media_image) {
      // If the media item exists, delete all the referenced image files.
      if ($image_references = $media_image->{$image_field}) {
        foreach ($image_references as $image_reference) {
          $file_id = $image_reference->get('target_id')->getValue();
          if ($referenced_file = $this->entityTypeManager->getStorage('file')->load($file_id)) {
            $referenced_file->delete();
          }
        }
      }
      // Remove the references to the images on the media item.
      $media_image->{$image_field} = NULL;
    }
    // Otherwise, create a new image media entity.
    else {
      // Create a media entity.
      $media_image = $media_storage->create([
        $image_title_field => $image_title,
        'bundle' => $image_media_type,
        'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      ]);
    }

    // Attach the image file. If there is no file, remove the image entity
    // and return NULL.
    $file = $this->saveImageFile($image, $image_title);
    if (is_object($file) && !empty($file->id())) {
      $media_image->set($image_image_field, [
        'target_id' => $file->id(),
        'alt' => $image_title,
      ]);
    }
    else {
      $media_image->delete();
      return NULL;
    }
    if ($uid = $this->config->get('programs.queue_user')) {
      $media_image->setOwnerId($uid);
    }
    $media_image->save();

    return $media_image->id();
  }

  /**
   * Creates a Drupal image file.
   *
   * @param string $image
   *   ArtworkUrl from Omny.
   * @param string $image_title
   *   The image title.
   *
   * @return \Drupal\file\FileInterface|false
   *   A file entity, or FALSE on error.
   */
  protected function saveImageFile(string $image, string $image_title): bool|FileInterface {

    // Create the directory from today's date as YYYY/MM/DD.
    $directory = 'public://weta_omny/' . date('Y/m/d');
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);

    // Download the file.
    try {
      $file_data = $this->client->request('GET', $image);
    }
    catch (\Exception $e) {
      if ($e->getMessage()) {
        $this->logger->error($this->t('There is no image at @url.', [
          '@url' => $image,
        ]));
      }
      return FALSE;
    }

    // Create the image name from the title.
    // The alias cleaner service checks for illegal characters and
    // also adds the initial slash.
    $alias_cleaner = \Drupal::service('pathauto.alias_cleaner');
    $name = $alias_cleaner->cleanAlias($image_title);

    // Convert to lowercase and replace spaces with hyphens.
    $name = strtolower($name);
    $name = str_replace(' ', '-', $name);

    // Get the file extension of the image.
    $parsed_image = parse_url($image);
    $image_path = $parsed_image['path'];
    $extension = pathinfo($image_path, PATHINFO_EXTENSION);

    // Put it all together.
    $image_name = $name . '.' . $extension;

    // Save the image.
    return \Drupal::service('file.repository')->writeData(
      $file_data->getBody(),
      $directory  . $image_name,
      FileSystemInterface::EXISTS_REPLACE
    );

  }

  /**
   * Gets all existing Term entities for a given vocabulary from the database.
   *
   * @param string $vocabulary
   *   Vocabulary machine name
   *
   * @return array
   *   Tag entities keyed by Name
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTerms(string $vocabulary): array {
    $definition = $this->entityTypeManager->getDefinition('taxonomy_term');
    $storage = $this->entityTypeManager->getStorage('taxonomy_term');
    /** @var \Drupal\taxonomy\Entity\Term[] $entities */
    $entities = $storage->loadByProperties([
      $definition->getKey('bundle') => $vocabulary,
    ]);
    // Re-key by name.
    $terms = [];
    foreach ($entities as $entity) {
      $terms[$entity->getName()] = $entity;
    }

    return $terms;
  }

  /**
   * Gets all existing Playlist entities, keyed by Omny ID.
   *
   * @return array
   *   Tag entities keyed by Omny ID
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPlaylists(): array {
    $config = $this->config;
    $vocabulary = $config->get('playlists.playlist_vocabulary');
    $playlist_id_field = $this->config->get('playlists.mappings.Id');
    $definition = $this->entityTypeManager->getDefinition('taxonomy_term');
    $storage = $this->entityTypeManager->getStorage('taxonomy_term');

    /** @var \Drupal\taxonomy\Entity\Term[] $entities */
    $entities = $storage->loadByProperties([
      $definition->getKey('bundle') => $vocabulary,
    ]);
    // Re-key by name.
    $terms = [];
    foreach ($entities as $entity) {
      $terms[$entity->get($playlist_id_field)->value] = $entity;
    }

    return $terms;
  }

  /**
   * Gets an existing term (creating a new one if necessary).
   *
   * This method also adds the newly created category to the
   * self::terms array so it can be reused with further objects
   * processed by this class.
   *
   * @param string $name
   *   Term name.
   *
   * @param string $vocabulary
   *   Vocabulary machine name
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   A term.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  protected function getOrAddTerm(string $name, string $vocabulary): EntityInterface {
    $terms = $this->getTerms($vocabulary);

    if (isset($terms[$name])) {
      $term = $terms[$name];
    }
    else {
      $definition = $this->entityTypeManager->getDefinition('taxonomy_term');
      /** @var \Drupal\taxonomy\Entity\Term $term */
      $term = Term::create([
        $definition->getKey('bundle') => $vocabulary,
      ]);
      $term->setName($name);

      // Do not create a revision.
      $term->setSyncing(TRUE);
      $term->save();
    }
    return $term;
  }

  /**
   * Gets an existing Playlist by Omny ID (creating a new one if necessary).
   *
   * This method also adds the newly created category to the
   * self::playlists array so it can be reused with further objects
   * processed by this class.
   *
   * @param string $id
   *   Playlist id from API.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\taxonomy\Entity\Term
   *   A term.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function getOrAddPlaylistById(string $id): EntityInterface|Term {
    $config = $this->config;
    $vocabulary = $config->get('playlists.playlist_vocabulary');
    $mappings = $config->get('playlists.mappings');
    $playlists = $this->getPlaylists();

    $playlist = $this->client->getPlaylist($id);


    if (isset($playlists[$playlist->Id])) {
      $term = $playlists[$playlist->Id];
    }
    else {
      $definition = $this->entityTypeManager->getDefinition('taxonomy_term');
      /** @var \Drupal\taxonomy\Entity\Term $term */
      $term = Term::create([
        $definition->getKey('bundle') => $vocabulary,
      ]);

      $term->setName($playlist->Title);
      $special_handling = ['Title', 'ProgramId', 'ArtworkUrl'];

      // Loop through the item properties (except those listed above)
      // and add them to the node based on their mappings if set.
      foreach ($playlist as $attribute => $value) {
        if (!in_array($attribute, $special_handling)) {
          if (!empty($playlist->{$attribute}) && isset($mappings[$attribute]) && $mappings[$attribute] != 'unused') {
            $term->set($mappings[$attribute], $value);
          }
        }
      }

      // Add the program reference, if configured.
      $program_ref_field = $this->config->get('playlists.references.program');
      $program_guid = $playlist->ProgramId;
      $program_bundle = $this->config->get('programs.drupal_program_content');
      if (!empty($program_guid) && !empty($program_bundle) && $program_ref_field != 'unused') {
        $program_node = $this->getNodeByGuid($program_guid, $program_bundle, 'programs');
        if (!empty($program_node) && !empty($program_node->id())) {
          $term->set($program_ref_field, ['target_id' => $program_node->id()]);
        }
      }

      // Process images.
      if (!empty($playlist->ArtworkUrl) && $mappings['ArtworkUrl'] != 'unused') {
        // Check to see if this item has an image already.
        $media_image = NULL;
        $image_field = $mappings['ArtworkUrl'];

        if ($term->get($image_field)->target_id) {
          // Get the existing image entity.
          $image_id = $term->get($image_field)->target_id;
          $media_storage = $this->entityTypeManager->getStorage('media');
          $media_image = $media_storage->load($image_id);
        }
        if ($media_id = $this->addOrUpdateMediaImage(
          $playlist->ArtworkUrl,
          $playlist->Title,
          'playlist',
          $media_image
        )) {
          $term->set($image_field, ['target_id' => $media_id]);
        }
      }

      // Set updated at field to the current time.
      $updated_at = self::getLatestUpdatedAt();
      $term->set(
        $mappings['updated_at'],
        $updated_at->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
      );

      // Do not create a revision.
      $term->setSyncing(TRUE);
      $term->save();
    }
    return $term;
  }

  /**
   * Determines what operation to perform on a Drupal entity based on API data.
   *
   * @param object $item
   *   Omny Studio API item (program, playlist, or clip).
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The Drupal entity (program, clip, or audio).
   * @param array $mappings
   *   The field mappings for this resource.
   * @param bool $force
   *   Whether to "force" the update.
   *
   * @return string
   *   The operation to be performed: 'created', 'updated', 'skipped', or
   *   'configured'. Other modules can set it to 'ignored'.
   */
  protected function getOperation(object $item, EntityInterface $entity, array $mappings, bool $force): string {

    // Default to skipped. This should only be unchanged resources.
    $operation = 'skipped';

    // Confirm that the title, id, and update hash fields are mapped.
    $title_field = '';
    if (isset($mappings['Name'])) {
      $title_field = $mappings['Name'];
    }
    elseif (isset($mappings['Title'])) {
      $title_field = $mappings['Title'];
    }

    $id_field = $mappings['Id'];
    $update_hash_field = $mappings['update_hash'];
    $drupal_update_hash = NULL;
    if ($entity->hasField($update_hash_field)) {
      $drupal_update_hash = $entity->get($update_hash_field)->value;
    }
    if (empty($title_field) || $title_field == 'unused' ||
      empty($id_field) || $id_field == 'unused' ||
      empty($update_hash_field) || $update_hash_field == 'unused') {
      $operation = 'configured';
    }
    // Always return 'created' for new entities.
    elseif ($entity->IsNew()) {
      $operation = 'created';
    }
    // If 'force' is requested and it is not a new entity, then the entity needs
    // to be updated.
    elseif ($force) {
      $operation = 'updated';
    }
    // Always return 'updated' if the updated_at field on the entity is empty.
    elseif (empty($drupal_update_hash)) {
      $operation = 'updated';
    }
    // For existing Drupal entities, check if the item has changed in PBS.
    elseif ($item->update_hash) {

      // If the Drupal hash does not equal the item has, then the entity needs to be updated.
      if ($item->update_hash !== $drupal_update_hash) {
        $operation = 'updated';
      }
    }

    // Allow other modules to alter the operation (e.g. change it to 'ignored').
    $context = [
      'item' => $item,
      'entity' => $entity,
    ];
    $this->moduleHandler->alter('weta_omny_operation', $operation, $context);

    return $operation;
  }

}

<?php


namespace Drupal\weta_omny;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use League\Uri\Components\Query;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use stdClass;

/**
 * Omny Studio API Client.
 *
 * @url https://api.omnystudio.com/api-docs/index
 *
 */
class ConsumerClient {

  /**
   * API endpoint base.
   */
  const CONFIG_BASE_URI = 'api.consumer_base_uri';

  /**
   * Config key for the Organization ID key.
   */
  const CONFIG_ORG_ID = 'api.organization_id';

  /**
   * Base endpoint.
   */
  const CONFIG_BASE_ENDPOINT = 'api.consumer_base_endpoint';


  /**
   * The Immutable Config for the Omny API settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * Response timeout in seconds.
   */
  const TIMEOUT = 300;

  /**
   * Connection timeout in seconds.
   */
  const CONNECT_TIMEOUT = 60;

  /**
   * Client for handling API requests
   *
   * @var GuzzleClient
   */
  protected GuzzleClient $client;

  /**
   * The Organization ID.
   * @var string
   */
  private mixed $orgId;

  /**
 * The API base uri.
 * @var string
 */
  private mixed $base_uri;

  /**
   * The API base endpoint.
   * @var string
   */
  private mixed $base_endpoint;


  /**
   * Client constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Drupal's config factory.
   * @param array $options
   *   Additional options to pass to Guzzle client.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    array $options = []
  ) {
    $this->config = $configFactory->get('weta_omny.settings');
    $this->orgId = $this->config->get(self::CONFIG_ORG_ID);
    $this->base_uri = $this->config->get(self::CONFIG_BASE_URI);
    $this->base_endpoint = $this->config->get(self::CONFIG_BASE_ENDPOINT);


    $options = [
        'base_uri' => $this->base_uri,
        'timeout' => self::TIMEOUT,
        // Connection timeout in seconds.
        'connect_timeout' => self::CONNECT_TIMEOUT,
      ] + $options;
    $this->client = new GuzzleClient($options);


  }

  /**
   * @param string $method
   *   Request method (e.g. 'get', 'post', 'put', etc.).
   * @param string $endpoint
   *   API endpoint to query.
   * @param array $query
   *   Additional query parameters in the form `param => value`.
   *
   * @return ResponseInterface|NULL
   *   Response data from the API.
   *
   */
  public function request(string $method, string $endpoint, array $query): ?ResponseInterface
  {
    try {
      $endpoint = $this->base_endpoint . $this->orgId . $endpoint;
      $response = $this->client->request($method, $endpoint, [
        'query' => self::buildQuery($query)
      ]);
    } catch (GuzzleException $e) {
      // If there is no transcript, the API returns 404. We need to resolve this gracefully or the entire import fails.
      if ($e->getCode() == 404) {
        return NULL;
      }
      else {
        throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
      }

    }

    switch ($response->getStatusCode()) {
      case 200:
      case 204:
        break;
      case 400:
      case 401:
      case 403:
      case 404:
      case 409:
      case 500:
      default:
        throw new RuntimeException($response->getReasonPhrase(), $response->getStatusCode());
    }

    return $response;
  }

  /**
   * Gets an iterator for paging through API responses.
   *
   * @param string $endpoint
   *   URL to query.
   * @param array $query
   *   Additional query parameters in the form `param => value`.
   * @param bool $headers
   *   Whether to include the headers in the response.
   *
   * @return string|stdClass|NULL
   *   A text string, or a JSON decoded object with response data.
   */
  public function get(string $endpoint, array $query = [], bool $headers = FALSE): string|stdClass|null {
    $response = $this->request('get', $endpoint, $query);
    // If there is no transcript, the API returns 404. We need to resolve this gracefully or the entire import fails.
    if (!$response) {
      return NULL;
    }
    if (isset($query['format']) && ($query['format'] == 'Text' || $query['format'] == 'TextWithTimestamps')) {
      $data = $response->getBody()->getContents();
    }
    else  {
      $data = json_decode($response->getBody()->getContents());
      if ($headers) {
        $data->headers = $response->getHeaders();
      }
    }

    return $data;

  }

  /**
   * Creates a query string from an array of parameters.
   *
   * @param array $parameters
   *   Query parameters keyed to convert to "key=value".
   *
   * @return string
   *   All parameters as a string.
   */
  public static function buildQuery(array $parameters): string
  {
    // Remove any empty parameters.
    $parameters = array_filter($parameters);
    $pairs = [];
    foreach ($parameters as $key => $value) {
      $pairs[] = [$key, $value];
    }
    $query = Query::createFromPairs($pairs);

    return (string) $query;
  }

  /**
   * @param string $id
   *   The Clip GUID.
   * @param string|null $format
   *   The format of the response.
   *
   * @return object|string|null The transcript object
   *   The transcript object
   */
  public function getTranscriptByClip(string $id, string $format = NULL): object|string|null {
    $endpoint = '/clips/' . $id . '/transcript';
    $query = NULL;
    if ($format) {
      $query = ['format' => $format];
    }
    return $this->get($endpoint, $query);
  }

}

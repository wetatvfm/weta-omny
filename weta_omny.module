<?php

/**
 * @file
 * Hooks for the Media Manager module.
 */

/**
 * Implements hook_cron().
 *
 * @see \Drupal\weta_omny\Plugin\QueueWorker\ShowsQueueWorker
 *
 * @throws \Exception
 */
function weta_omny_cron() {

  /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
  $config_factory = \Drupal::service('config.factory');
  $config = $config_factory->get('weta_omny.settings');
  /** @var \Drupal\weta_omny\ProgramManager $programManager */
  $programManager = Drupal::service('weta_omny.program_manager');
  /** @var \Drupal\weta_omny\ClipManager $clipManager */
  $clipManager = Drupal::service('weta_omny.clip_manager');

  $request_time = Drupal::time()->getRequestTime();
  $now = new DateTime("@{$request_time}");

  if ($config->get($programManager->getAutoUpdateConfigName())) {

    // Check the "Queue builder update interval" from the "Program Mappings" form.
    $interval = (int) $config->get($programManager->getAutoUpdateIntervalConfigName());
    // Check how long it has been since all (subscribed) programs were updated.
    $diff = $now->getTimestamp() - $programManager->getLastUpdateTime()->getTimestamp();

    // Update all (subscribed) programs, if necessary.
    if ($diff > $interval) {
      $programManager->updateQueue();
    }

    // Check the "Queue builder update interval" from the "Clip Mappings" form.
    $interval = (int) $config->get($clipManager->getAutoUpdateIntervalConfigName());
    // Check how long it has been since all (subscribed) programs were updated.
    $diff = $now->getTimestamp() - $clipManager->getLastUpdateTime()->getTimestamp();

    // Update all clips, if necessary.
    if ($diff > $interval) {
      $clipManager->updateQueue();
    }
  }
}

/**
 * Implements hook_theme().
 */
function weta_omny_theme() {
  return [
    'weta_omny_embed' => [
      'variables' => [
        'src' => NULL,
        'title' => NULL
      ],
      'template' => 'weta-omny-embed',
    ],
    'weta_omny_playlist_embed' => [
      'variables' => [
        'src' => NULL,
        'title' => NULL
      ],
      'template' => 'weta-omny-playlist-embed',
    ],
  ];
}
